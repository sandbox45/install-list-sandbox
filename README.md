# Install List Sandbox

A demonstration of the [Fix UnmetDependenciesException when module lists itself in a config dependency](https://www.drupal.org/project/drupal/issues/3178993).

Clone this repo, and run:

```
composer install
drush site-install tryme --db-url=mysql://drupal8:drupal8@database/drupal8 -y
```

> Can be run with Lando.
> ```
> lando start
> lando composer install
> lando drush site-install tryme --db-url=mysql://drupal8:drupal8@database/drupal8 -y
> ```
